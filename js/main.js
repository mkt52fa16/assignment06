/*
* Megan Tice
*/

/*
* Image switching Function and Variables
*/

// functions for switching images
    // next button functions
    function next() {
    "use strict";
    imgCount++ ;
    if(imgCount > totalImgs) imgCount = 0
    document.getElementById("pictures").src = imageGallery[imgCount] ;
}
    // previous button functions
    function previous() {
    imgCount--;
    if(imgCount < 0) imgCount = totalImgs ;
    document.getElementById("pictures").src = imageGallery[imgCount] ;      
}

    // variables for images in array
    var imageGallery = ["images/dunes.jpg", "images/jetty.jpg", "images/ocean.jpg", "images/shells.jpg"];
    var imgCount = 0;
    var totalImgs = imageGallery.length - 1;


/*
* Day, Month and Year Function and Variable on page load
*/

window.onload = function date_and_season() {
    "use strict";
    //  variables for the date
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        
    // date and month change function
    full_date = new Date(),
    current_month = monthNames[full_date.getMonth()],
        
    // generate current date and year,
    current_day = full_date.getDate(),
    current_year = full_date.getFullYear(),
    
    // display of date in the footer of page
    new_span = document.createElement('span'),
    new_text = document.createTextNode(current_day + ' ' + current_month + ', ' + current_year),
    position = document.getElementsByTagName('footer')[0];
    
    // send the date to footer
    new_span.appendChild(new_text);
    position.appendChild(new_span);
}
    



    
   


